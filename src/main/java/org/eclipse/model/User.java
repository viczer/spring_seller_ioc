package org.eclipse.model;

import java.util.ArrayList;
import java.util.List;

public class User {
	private int id;
	private String nom;
	private String prenom;
	private Order order;
	List<String> paymentMethod;
//	public User() {
//		super();
//		// TODO Auto-generated constructor stub
//	}
//	
//	public User(int id, String nom, String prenom, Order order
//			List<String> paymentMethod) {
//		
//		this.id = id;
//		this.nom = nom;
//		this.prenom = prenom;
//		this.order = order;
//		this.paymentMethod = paymentMethod;
//	}

	public List<String> getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(List<String> paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public int getId() {
		return id;
	}
	public Order getOrder() {
		return order;
	}
	public void setOrder(Order order) {
		this.order = order;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	
	
	@Override
	public String toString() {
		return "User [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", order=" + order + ", paymentMethod="
				+ paymentMethod + "]";
	}
	public void afficher() {

		System.out.println("User [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", order=" + order + ", paymentMethod="
				+ paymentMethod + "]");
//		System.out.println("Mes sport :");
//		sports.forEach(System.out::println);
	}
	

}
